<?php
/*
Plugin Name: Blue inventory
Plugin URI: 
Description: Blue inventory woocommerce
Version: 1.1
Author: AgileSolutionspk
Text Domain: my-plugin
Author URI: http://agilesolutionspk.com/
*/
if ( !class_exists( 'Agile_Blue_inventory' )){
	class Agile_Blue_inventory{
		function __construct() {
			register_activation_hook( __FILE__, array($this, 'install') );
			register_deactivation_hook( __FILE__, array($this, 'de_activate') );
			add_shortcode( 'agile_contact7', array(&$this, 'agile_contact_usform') );
			add_action( 'wp_enqueue_scripts',  array(&$this, 'agile_contact_enque') );
			add_action( 'plugins_loaded', array(&$this,'myplugin_load_textdomain' ));
			add_action( 'admin_menu', array(&$this, 'addAdminPage') );
			add_action( 'admin_menu', array(&$this, 'testpage') );
			
		}
		function testpage(){
			add_menu_page('blue-testing', 'blue-testing', 'manage_options', 'blue_invent_testing', array(&$this, 'agile_testing'));
		}
		function agile_testing(){
			$all_products = 'productttttss';
			$prod_title = 'bmw car';
			$meta_values = 'gfggfgfgfg';
			//echo $this->agile_inventory_insert($all_products ,$prod_title,$meta_values);
		}
		function install(){
			
		}
		function de_activate(){
			
		}
		function addAdminPage(){
			add_menu_page('blue-inventory', 'blue-inventory', 'manage_options', 'blue_invent', array(&$this, 'agile_inventory_pro'));
		}
		function agile_inventory_pro(){
			
			$args = array(
				'post_type'        => 'product' ,
				'post_status'      => 'publish');
			$invt_prd = get_posts( $args );
			foreach($invt_prd as $x){
			
				$product_terms = wp_get_object_terms($x->ID, 'product_type');
				alog('$product_terms',$product_terms,__FILE__,__LINE__);
				$product_type = $product_terms[0]->name;
				alog('$product_catg',$product_type,__FILE__,__LINE__);
				//switch($product_type)
				
					$meta_values = get_post_meta( $x->ID );
					$prod_title = $x->post_title;
					$all_products = $this->get_invt_product($meta_values);
					alog('$all_products',$all_products,__FILE__,__LINE__);
					//for each
						$id = $this->agile_inventory_insert( $all_products ,$prod_title);
					alog('$id123',$id,__FILE__,__LINE__);
						$this->set_product_meta($id, $all_products);
				
			}
			return $all_products;
		}
		function get_invt_product($meta_values){ 
			//returns all items
			$inventory_attributes = array( );
			$ignore_attributes = array('_edit_lock','_edit_last','_visibility' );
			foreach($meta_values as  $k => $val){
				echo "<pre>";
				echo "key=".$k;
				echo "val=".$val[0];
				if(in_array($k,$ignore_attributes)){
					continue ;
				}
				$inventory_attributes[$k]=$val[0];
			}
			alog('$inventory_attributes',$inventory_attributes,__FILE__,__LINE__);
			print_r($inventory_attributes);
			return $meta_values;
		}
		function agile_inventory_insert($all_products ,$prod_title){
			//inserts single item
			$my_post = array(
			  'post_title'    => $prod_title,
			  'post_status'   => 'publish',
			  'post_type'   => 'product'
			);

			$my_post123 = wp_insert_post( $my_post );
			return $my_post123;
			//alog('post_product',$my_post,__FILE__,__LINE__);
			//alog('post_product123',$my_post123,__FILE__,__LINE__);
			
		}
		function set_product_meta($id, $all_products){
		
		//alog('$all_products5555344', $all_products,__FILE__,__LINE__);
		
		
			//foreach
			foreach($all_products as  $key => $val){
			//update post meta
				$update_product = update_post_meta($id, $key, $val[0]);
				//alog('$update_product', $update_product,__FILE__,__LINE__);
				
			}
		}
		function myplugin_load_textdomain(){
			 load_plugin_textdomain( 'my-plugin', false, dirname( plugin_basename( __FILE__ ) ) . '/langs/' ); 
		}
		function agile_contact_enque(){
			wp_enqueue_script( 'jjjs-bootstrap-js', plugins_url( 'js/tcal.js', __FILE__ ) );
			wp_enqueue_style( 'cccss-bootstrap-css', plugins_url( 'css/tcal.css', __FILE__ ) );
		}
		function agile_contact_usform(){
			
			return __( 'hello world', 'my-plugin' );
			
		}
	}
}

$proo_invt = new Agile_Blue_inventory();