<?

if ( !class_exists( 'Blue_Inventory_Wp_Api' )){
	class Blue_Inventory_Wp_Api{
		private $api_key;
		private $store_key;
		private $end_point;
		
		
		function __construct($api_key, $store_key, $end_point) {
			//put validation and throw error.  length and end should have http:// or https://
			
			$this->api_key = $api_key;
			$this->store_key = $store_key;
			$this->end_point = $end_point;
			
		}
		
		//$param is the key/value array 
		public function request($method, $param){
			$param['api_key'] = $this->api_key;
			$param['store_key'] = $this->store_key;
			$param['method'] = $method;
			
			$args = array(
						'method' => 'POST',
						'timeout' => 45,
						'redirection' => 5,
						'httpversion' => '1.0',
						'blocking' => true,
						'headers' => array(),
						'body' => $param,
						'cookies' => array()
					);
					
			$response = wp_remote_post( $this->end_point, $args );
			echo "<pre>";print_r($response);
			if ( is_wp_error( $response ) ) {
			   $error_message = $response->get_error_message();
			   return "Something went wrong: $error_message";
			}
		}
		
		
	} //class ends
} //class exists ends

